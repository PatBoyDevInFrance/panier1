<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\RecherchePrix;
use App\Form\AdminType;
use App\Form\RecherParPrixType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ProductRepository $repo, PaginatorInterface $paginatorInterface, Request $request): Response
    {
        $recherche = new RecherchePrix();
        $form = $this->createForm(RecherParPrixType::class, $recherche);
        $form->handleRequest($request);

        $cartes = $paginatorInterface->paginate(
            $repo->findWithPagination($recherche), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );

        return $this->render('product/product.html.twig', [
            'cartes' => $cartes,
            'form' => $form->createView(),
            'admin' => true
        ]);
    }
    
    /**
     * @Route("/admin/creation", name="creationCarte")
     * @Route("/admin/{id}", name="modifAdmin")
     */
   public function modifier(Product $product = null, Request $request, EntityManagerInterface $em ): Response
   {    
       if(!$product){
           $product = new Product();
       }
        
        $form = $this->createForm(AdminType::class,$product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->persist($product);
            $em->flush();
            $this->addFlash('success', "l'action a été éffectué");
            return $this->redirectToRoute('admin');

        }

        return $this->render('admin/index.html.twig', [
            'cartes' => $product,
            'form' => $form->createView(),

        ]);
   }

   
}
