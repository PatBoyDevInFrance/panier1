<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\Compteur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/panier", name="cart")
     */
    public function index(Cart $cart, Compteur $compteur): Response
    {       

          
        return $this->render('cart/index.html.twig',[
            'items' => $cart->getFullCart(),
            'total'=> $cart->getTotal(),
            'totalQ'=> $cart->getQuantity()
        ]);
    
    }

    function nombre_vues_detail_mois(Cart $cart,int $annee, int $mois): array {
        $mois = str_pad($mois, 2, '0', STR_PAD_LEFT);
        $fichier = dirname(__DIR__). DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'compteur' . $annee . '-' . $mois . '-' . '*';
        $fichiers = glob($fichier);
        $visites = [];
        foreach($fichiers as $fichier){
            $parties = explode('-', basename($fichier));
            $visites[] = 
            [
                'annee' => $parties[1],
                'mois' => $parties[2],
                'jour' => $parties[3],
                'visites' => file_get_contents($fichier)
            ];
                
        }
   
        return $visites;
    }
   

    /**
     * @Route("/panier/add/{id}", name="cart_add" )
     */
    public function add($id, Cart $cart): Response
    {       
        $cart->add($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/decrease/{id}", name="decrease_to_cart")
     */
    public function decrease ($id, Cart $cart): Response
    {       
        $cart->decrease($id);

        return $this->redirectToRoute('cart');
    }

}
