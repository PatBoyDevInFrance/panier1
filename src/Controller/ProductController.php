<?php

namespace App\Controller;

use App\DataFixtures\Product;
use App\Entity\RecherchePrix;
use App\Form\RecherParPrixType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/client/product", name="product")
     */
    public function index(ProductRepository $repo, PaginatorInterface $paginatorInterface, Request $request): Response
    {   
        $recherche = new RecherchePrix();
        $form = $this->createForm(RecherParPrixType::class, $recherche);
        $form->handleRequest($request);

        $cartes = $paginatorInterface->paginate(
            $repo->findWithPagination($recherche), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );

        return $this->render('product/product.html.twig', [
            'cartes' => $cartes,
            'form' => $form->createView(),
            'admin' => false
        ]);
    }

    
    
}
