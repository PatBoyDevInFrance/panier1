<?php

namespace App\DataFixtures;

use App\Entity\Product as EntityProduct;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Product extends Fixture
{
    public function load(ObjectManager $manager)
    {   
        $c1 = new EntityProduct();
        $c1->setNom("Bien 1");
        $c1->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c1->setImage("img1.jpg");
        $c1->setPrice(11193000);
        // $product = new Product();
        $manager->persist($c1);

        $c2 = new EntityProduct();
        $c2->setNom("Bien 2");
        $c2->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c2->setImage("img2.jpg");
        $c2->setPrice(823000);
        // $product = new Product();
        $manager->persist($c2);

        $c3 = new EntityProduct();
        $c3->setNom("Bien 3");
        $c3->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c3->setImage("img3.jpg");
        $c3->setPrice(230008);
        // $product = new Product();
        $manager->persist($c3);

        $c4 = new EntityProduct();
        $c4->setNom("Bien 4");
        $c4->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c4->setImage("img4.jpg");
        $c4->setPrice(176000);
        // $product = new Product();
        $manager->persist($c4);

        $c5 = new EntityProduct();
        $c5->setNom("Bien 5");
        $c5->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c5->setImage("img5.jpg");
        $c5->setPrice(125600);
        // $product = new Product();
        $manager->persist($c5);


        $c6 = new EntityProduct();
        $c6->setNom("Bien 6");
        $c6->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c6->setImage("img6.jpg");
        $c6->setPrice(823000);
        // $product = new Product();
        $manager->persist($c6);


        $c7 = new EntityProduct();
        $c7->setNom("Bien 7");
        $c7->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c7->setImage("img7.jpg");
        $c7->setPrice(723000);
        // $product = new Product();
        $manager->persist($c7);

        $c8 = new EntityProduct();
        $c8->setNom("Bien 8");
        $c8->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c8->setImage("img8.jpg");
        $c8->setPrice(623000);
        // $product = new Product();
        $manager->persist($c8);

        $c9 = new EntityProduct();
        $c9->setNom("Bien 9");
        $c9->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c9->setImage("img9.jpg");
        $c9->setPrice(423000);
        // $product = new Product();
        $manager->persist($c9);

        $c10 = new EntityProduct();
        $c10->setNom("Bien 10");
        $c10->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c10->setImage("img10.jpg");
        $c10->setPrice(23000);
        // $product = new Product();
        $manager->persist($c10);

        $c11 = new EntityProduct();
        $c11->setNom("Bien 11");
        $c11->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c11->setImage("img11.jpg");
        $c11->setPrice(223000);
        // $product = new Product();
        $manager->persist($c11);

        $c12 = new EntityProduct();
        $c12->setNom("Bien 12");
        $c12->setDescription("Rerum unde est temporibus quas sunt atque eum. Dolores accusantium cum quia non quia deserunt quidem. Dolor voluptates dolorem doloremque consequatur sunt voluptate et recusandae.");
        $c12->setImage("img12.jpg");
        $c12->setPrice(323000);
        // $product = new Product();
        $manager->persist($c12);

        $manager->flush();
    }
}
