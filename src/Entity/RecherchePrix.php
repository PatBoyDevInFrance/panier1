<?php

namespace App\Entity;

class RecherchePrix{

    private $prixMin;
    private $prixMax;

    public function getPrixMin(){return $this->prixMin;}
    public function setPrixMin(int $prix){$this->prixMin= $prix; return $this;}

    public function getPrixMax(){return $this->prixMax;}
    public function setPrixMax(int $prix){$this->prixMax= $prix; return $this;}

}